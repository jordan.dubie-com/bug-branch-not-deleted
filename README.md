# Steps to reproduce

- Go to Setting → Merge Request → Very "Delete source branch" option is enabled
- Create a branch
- Create a merge request (leave all by default)
- Enable "squash commits" and "delete source branch" options
- Merge the Merge Request
- The branch is not deleted after the merge